package com.example.documentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class DokumenResponse implements Serializable {
    private Long idDokumen;
    private String kodeDokumen;
    private String deskripsi;
    private String tipeDokumen;
    private String kodeProduk;
    private String namaJasper;
    private String serviceHandler;
}
