package com.example.documentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseService {
    Object responseCode;
    Object responseDesc;
    Object responseData;
}
