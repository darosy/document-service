package com.example.documentservice.services.document;

import com.example.documentservice.dto.DokumenResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class DocumentAsyncService {

    private final ApplicationContext applicationContext;

    @Async
    public void execute(String userId, DokumenResponse dokumen) throws Exception {
        String serviceHandler = dokumen.getServiceHandler();
        if (serviceHandler == null)
            throw new Exception("System dokumen handler must be define for " + dokumen.getIdDokumen());

        try {
            DocumentHandler handler = (DocumentHandler) applicationContext.getBean(serviceHandler);
            handler.execute(userId, dokumen);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("End execute dokumen "+"-" + userId + "-" + dokumen);
    }

}
