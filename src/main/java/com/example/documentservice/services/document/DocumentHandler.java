package com.example.documentservice.services.document;

import com.example.documentservice.dto.DokumenResponse;

public interface DocumentHandler {

    void execute(String userId, DokumenResponse dokumen) throws Exception;

}
