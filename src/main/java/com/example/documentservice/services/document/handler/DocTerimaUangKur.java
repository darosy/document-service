package com.example.documentservice.services.document.handler;

import com.example.documentservice.dto.DokumenResponse;
import com.example.documentservice.services.document.DocumentHandler;
import com.example.documentservice.services.document.DocumentServices;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;

@Slf4j
@Lazy
@RequiredArgsConstructor
@Component("KurTerimaUang")
public class DocTerimaUangKur implements DocumentHandler {

    private final DocumentServices documentService;

    @Override
    public void execute(String userId, DokumenResponse dokumen) throws Exception {
        log.info("Start DocKurTerimaUang");
        HashMap<String, Object> data = new HashMap<>();
        data.put("createdBy", "system");
        data.put("namaKasir", "");
        data.put("nikKasir", "");
        data.put("namaNasabah", "");
        data.put("tglCetak", new Date());

        documentService.printUploadSaveDokumen("", dokumen, data, null);

        log.info("End DocKurTerimaUang");
    }
}
