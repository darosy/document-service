package com.example.documentservice.services.document;

import com.example.documentservice.dto.DokumenResponse;
import com.example.documentservice.services.MinioService;
import com.example.documentservice.services.ReportService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
@RequiredArgsConstructor
public class DocumentServices {

    private final MinioService minioService;
    private final DocumentAsyncService documentAsyncService;

    @Value("${minio.bucket.name.doc}")
    private String bucketName;

    public boolean doCetak(HashMap<String, Object> hData) {
        try {
            DokumenResponse dokumenResponse = new DokumenResponse();
            dokumenResponse.setServiceHandler("KurTerimaUang");
            dokumenResponse.setNamaJasper("kur_bukti_terima_uang");
            documentAsyncService.execute("SYSTEM", dokumenResponse);
        } catch (Exception e) {
            return true; // by pass generate dokumen
        }
        return true;
    }


    private CompletableFuture<String> generateFileAndUpload(String fileName, InputStream inputStream, HashMap<String, Object> parameters, JRBeanCollectionDataSource jrBeanCollectionDataSource) throws Exception {
        return CompletableFuture.supplyAsync(() -> {
            ByteArrayOutputStream baos = null;
            try {
                if (jrBeanCollectionDataSource == null) {
                    baos = new ReportService().generateReport(inputStream, parameters, null);
                } else {
                    baos = new ReportService().generateReport(inputStream, parameters, jrBeanCollectionDataSource);
                }
            } catch (Exception ignored) {

            }
            return baos;
        }).thenApplyAsync(baos -> {
            String url = null;
            if (baos != null) {
                try {
                    url = minioService.uploadFile(baos, bucketName, fileName);
                } catch (Exception ignored) {
                }
            }
            return url;
        }).exceptionally(e -> {
            log.error(e.getMessage(), e);
            return null;
        });
    }

    public void printUploadSaveDokumen(String nomorAplikasi, DokumenResponse dokumen, HashMap<String, Object> data, JRBeanCollectionDataSource dataSource) throws Exception {
        String namaJasper = dokumen.getNamaJasper() + ".jasper";
        InputStream fileStream = this.getClass().getResourceAsStream("/report/" + namaJasper);
        if (fileStream == null) {
            fileStream = this.getClass().getClassLoader().getResourceAsStream("/report/" + namaJasper);
        }

        String namaFilePDf = nomorAplikasi + "_" + namaJasper.replaceAll(".jasper", ".pdf");
        log.info("Start Process Print PDF : {} ", namaFilePDf);

        CompletableFuture<String> fileUrl = generateFileAndUpload(namaFilePDf, fileStream, data, dataSource);
        if (fileUrl.get().isEmpty() || fileUrl.get().isBlank()) {
            throw new Exception("File uploaded failed miserably : {} " + fileUrl.get());
        }
        log.info("File uploaded successfully > filename: {} & filepath: {}", namaFilePDf, fileUrl.get());
        saveKreditDokumen(nomorAplikasi, namaFilePDf, dokumen);
    }

    private void saveKreditDokumen(String noAplikasi, String namaFile, DokumenResponse dokumen) {
        try {
            // TODO
//            kreditDokumenDao.save(KreditDokumen.builder()
//                .kodeDokumen(dokumen.getKodeDokumen())
//                .namaFile(namaFile)
//                .deskripsi(dokumen.getDeskripsi())
//                .nomorAplikasi(noAplikasi)
//                .build());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    public String getDokumen(String namaFile) throws Exception {
        String url;
        try {
            url = minioService.getMinioUrl(null, namaFile, bucketName);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return url;
    }
}
