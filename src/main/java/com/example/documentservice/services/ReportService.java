package com.example.documentservice.services;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReportService {
    private static final Logger log = Logger.getLogger(ReportService.class.getName());

    public ReportService() {
    }

    public ByteArrayOutputStream generateReport(InputStream in, HashMap<String, Object> param, JRDataSource jrDataSource) throws Exception {
        ByteArrayOutputStream outputStream;

        try {
            JasperPrint jasperPrint = JasperFillManager.fillReport(in, param, jrDataSource != null ? jrDataSource : new JREmptyDataSource());

            outputStream = new ByteArrayOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            SimplePdfExporterConfiguration reportConfig = new SimplePdfExporterConfiguration();
            reportConfig.setEncrypted(true);
            reportConfig.setOwnerPassword(String.valueOf(System.nanoTime()));
            reportConfig.setUserPassword("");
            reportConfig.setPermissions(4);
            exporter.setConfiguration(reportConfig);
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
            exporter.exportReport();
            log.info("Report successfully generated");
            return outputStream;
        } catch (Exception var8) {
            log.log(Level.SEVERE, var8.getMessage(), var8);
            throw var8;
        }
    }
}
