package com.example.documentservice.services;

import com.example.documentservice.util.Constants;
import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings({"deprecated"})
public class MinioService {

    private final OkHttpClient losOkHttpClient;

    @Value("${minio.url}")
    private String minioUrl;

    @Value("${apim.url}")
    private String apimUrl;

    @Value("${minio.port}")
    private String minioPort;

    @Value("${minio.region}")
    private String minioRegion;

    @Value("${minio.id}")
    private String minioAccess;

    @Value("${minio.pass}")
    private String minioSecret;

    @Value("${minio.presigned.expiretime}")
    private String minioExpire;

    @Value("${minio.max.file.size}")
    private String maxSize;

    public String upload(MultipartFile file, String bucketName) throws Exception {
        String urlFiles;
        if (file.isEmpty()) throw new Exception("File not found");

        int fileMax = Integer.parseInt(maxSize);

        log.info("File Size : " + file.getSize());

        if (file.getSize() >= (fileMax * 1024L))
            throw new Exception("Max Image Size " + fileMax / 1024 + "MB");

        String fileExtension = Objects.requireNonNull(file.getContentType()).toLowerCase();
        log.info("File Extension : " + fileExtension);

        if (!(fileExtension.equals(Constants.IMAGE_EXTENSION.JPG) ||
                fileExtension.equals(Constants.IMAGE_EXTENSION.ALLIMG) ||
                fileExtension.equals(Constants.IMAGE_EXTENSION.JPEG) ||
                fileExtension.equals(Constants.IMAGE_EXTENSION.IMG) ||
                fileExtension.equals(Constants.IMAGE_EXTENSION.PNG))) {
            throw new Exception("File Image Only (jpg,jpeg,img,png)");
        }

        MinioClient minioClient = getMinioClient();

        if (!minioClient.bucketExists(bucketName)) {
            log.info("Creating new bucket : " + bucketName);
            minioClient.makeBucket(bucketName);
        }

        // Create a InputStream for object upload.
        ByteArrayInputStream inputStream = new ByteArrayInputStream(file.getBytes());

        minioClient.putObject(bucketName, file.getOriginalFilename(), inputStream, inputStream.available(), fileExtension);

        // close
        inputStream.close();

        // get url
        urlFiles = getMinioUrl(minioClient, file.getOriginalFilename(), bucketName);
        log.info(file.getOriginalFilename() + " is uploaded successfully");
        return urlFiles;
    }

    public String uploadThumbnails(MultipartFile file, String bucketName) throws Exception {
        String urlFiles;
        if (file.isEmpty()) throw new Exception("File not found");

        String fileExtension = Objects.requireNonNull(file.getContentType()).toLowerCase();
        log.info("File Extension : " + fileExtension);

        MinioClient minioClient = getMinioClient();

        if (!minioClient.bucketExists(bucketName)) {
            log.info("Creating new bucket : " + bucketName);
            minioClient.makeBucket(bucketName);
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        BufferedImage img = ImageIO.read(file.getInputStream());
        BufferedImage bufferedImage = scale(img);
        ImageIO.write(bufferedImage, "jpg", baos);

        // Create a InputStream for object upload.
        ByteArrayInputStream inputStream = new ByteArrayInputStream(baos.toByteArray());

        minioClient.putObject(bucketName, file.getOriginalFilename(), inputStream, inputStream.available(), fileExtension);

        // close
        inputStream.close();

        // get url
        urlFiles = getMinioUrl(minioClient, file.getOriginalFilename(), bucketName);
        log.info(file.getOriginalFilename() + " is uploaded successfully");
        return urlFiles;
    }

    public String uploadFile(ByteArrayOutputStream outputStream, String bucketName, String fileName) throws Exception {
        String urlFiles;
        if (outputStream == null) throw new Exception("File not found");

        String fileExtension = FilenameUtils.getExtension(fileName);

        // String PDF = "pdf";
        if (!"pdf".equalsIgnoreCase(fileExtension)) throw new Exception("Pdf File Only");

        MinioClient minioClient = getMinioClient();

        if (bucketName.isEmpty() || bucketName.isBlank())
            bucketName = "test123";

        if (!minioClient.bucketExists(bucketName)) {
            log.info("Creating new bucket : " + bucketName);
            minioClient.makeBucket(bucketName);
        }

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("title", fileName);
        headerMap.put("author", "Darosy");
        headerMap.put("dateCreated", new SimpleDateFormat("yyyyMMdd").format(new Date()));
        headerMap.put("Content-Type", "application/pdf");

        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        minioClient.putObject(bucketName, fileName, inputStream, Long.valueOf(inputStream.available()), headerMap, null, "application/pdf");

        // close
        inputStream.close();

        // get url
        urlFiles = getMinioUrl(minioClient, fileName, bucketName);
        log.info(fileName + " is uploaded successfully");
        return urlFiles;
    }

    private BufferedImage scale(BufferedImage sourceImage) {
        BufferedImage thumb = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = (Graphics2D) thumb.getGraphics();
        g2d.drawImage(sourceImage, 0, 0,
                thumb.getWidth() - 1,
                thumb.getHeight() - 1,
                0, 0,
                sourceImage.getWidth() - 1,
                sourceImage.getHeight() - 1,
                null);
        g2d.dispose();
        return thumb;
    }

    public String getMinioUrl(MinioClient minioClient, String namaFile, String bucketName) throws Exception {
        String urlFiles;
        if (minioClient == null) {
            minioClient = getMinioClient();
        }
        urlFiles = minioClient.presignedGetObject(bucketName, namaFile, Integer.valueOf(minioExpire));
//        return urlFiles.replace(minioUrl+":"+minioPort, apimUrl);
        return urlFiles;
    }

    public void delete(String fileName, String bucketName) throws Exception {
        MinioClient minioClient = getMinioClient();
        if (!minioClient.bucketExists(bucketName)) {
            log.info("Error Bucket : " + bucketName + " not found!");
            throw new Exception("Error Bucket : " + bucketName + " not found!");
        }
        minioClient.removeObject(bucketName, fileName);
        log.info(fileName + " is deleted successfully");
    }

    private MinioClient getMinioClient() throws InvalidPortException, InvalidEndpointException {
        return new MinioClient(minioUrl, Integer.parseInt(minioPort), minioAccess, minioSecret, minioRegion, true, losOkHttpClient);
    }

}
