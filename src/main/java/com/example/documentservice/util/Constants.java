package com.example.documentservice.util;

public class Constants {

    public interface IMAGE_EXTENSION {
        String JPG = "image/jpg";
        String PNG = "image/png";
        String JPEG = "image/jpeg";
        String IMG = "image/img";
        String ALLIMG = "image/*";
    }
}
